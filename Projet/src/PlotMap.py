#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 16:30:50 2018

@author: 3703554
"""

from gurobipy import * 
from subprocess import call

class PlotMap:
    def __init__(self,fImage,x,v,sects,q):
        self.img = fImage
        self.solution=x
        self.villes=v
	self.secteurs=sects
	self.q=q
        
        
    def getMap(self):
	xy=[]
        k=0
        for j in self.secteurs:
	    tmp=""
            for i in range(len(self.solution)):
		if self.q==1:
                    if self.solution[i][j].x !=0:
                        c1=self.villes[i][0]
                        c2=self.villes[i][1]
                        tmp+=str(c1)+'-'+str(c2)+"+"
		if self.q==0:
                    if self.solution[i][k].x !=0:
                        c1=self.villes[i][0]
                        c2=self.villes[i][1]
                        tmp+=str(c1)+'-'+str(c2)+"+"
	    xy.append(tmp)
	    k+=1
	arr=','.join(xy)
	sects=[]
	for i in self.secteurs:
	    sects.append(str(self.villes[i][0])+'-'+str(self.villes[i][1]))
        exit_code = call("python3 maker.py "+arr+" "+','.join(sects)+" "+self.img, shell=True)
        
        
