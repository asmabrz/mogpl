#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 17 07:51:33 2018

@author: asma
"""
from gurobipy import * 
from Departement import *
from PL import *
from PlotMap import *
import numpy as np
import time

class Builder:
    def __init__(self,fImg,departement):
      self.alpha=0.1
      self.fImage=fImg
      self.dep=departement
      self.n=self.dep.getNbVilles()
      
      
      
    def question3(self,k):
        cities=self.dep.getDistV()
        self.k=k
        self.cst=self.constante(k)
        self.pl=PL(self.n,self.k,1)
        self.pl.addVarX(self.n,self.n)
        self.pl.addVarZ()
        self.pl.addVarR()
        self.const1(self.n) 
        self.const2(self.n)
        self.const4() 
        self.const3(self.dep.getDistV())
        #define objective
        self.pl.const5()
        self.pl.defineObjective2(GRB.MINIMIZE,cities,self.n,self.n)
        #generate solution
    	t1=time.time()
        self.optimize()
    	t2=time.time()
    	print "Temps d'execution: "+ str(t2-t1)
        secteurs=self.sectorsGenerated()         
        self.solutionConcrete(1,secteurs)
	self.getSolution(secteurs,1)
        matrix=self.distanceS_V(1,secteurs)
        self.pl.m.write("file.lp")
        self.meanSatisfactionPerSector(1,secteurs)
    	self.getMayorLessSat(matrix,secteurs)


        
    def question2(self,sects):
        self.sectors=sects
        self.k=k=len(self.sectors)
        self.cst=self.constante(k)
        self.pl=PL(self.n,self.k,0)
        self.pl.addVarX(self.n,self.k)
        self.pl.addVarZ()
        self.const1(len(self.sectors))
        self.const2(len(self.sectors))
        self.const3(self.fx(self.sectors))
        #define objective
        cities=self.dep.getDistV()
        self.pl.defineObjective2(GRB.MINIMIZE,cities,self.n,self.k)
        #generate solution
	t1=time.time()
        self.optimize()
        t2=time.time()
    	print "Temps d'execution: "+ str(t2-t1)
        self.solutionConcrete(0,sects)
	self.getSolution(sects,0)
        matrix=self.distanceS_V(0,sects)
        self.meanSatisfactionPerSector(0,sects)
        self.getMayorLessSat(matrix,sects)
        self.pl.m.write("file.lp")

    def question1(self,sects):
        self.sectors=sects
        self.k=k=len(self.sectors)
        self.cst=self.constante(k)
        self.pl=PL(self.n,self.k,0)
        self.pl.addVarX(self.n,self.k)
        self.const1(len(self.sectors))
        self.const2(len(self.sectors))
        #define objective
        self.pl.defineObjective1(GRB.MINIMIZE,self.fx(self.sectors))
        #generate solution
        t1=time.time()
        self.optimize()
        t2=time.time()
    	print "Temps d'execution: "+ str(t2-t1)
        self.solutionConcrete(0,sects)
	self.getSolution(sects,0)
        matrix=self.distanceS_V(0,sects)
        self.meanSatisfactionPerSector(0,sects)
        self.getMayorLessSat(matrix,sects)
        self.pl.m.write("file.lp")

#sum of the total distances between cities and its sector
    def valuateSolution(self,fx):
        somme=0
        for i in range(len(fx)):
            for j in range(len(fx[i])):
                somme+=fx[i][j]*self.dep.getDistV()[i][self.sectors[j]]
        return somme
                
    def optimize(self):
        self.pl.optimize()
  
    
    def const1(self,mySectors):
        #A city belongs to a single service sector
        for i in range(self.n):
            if (self.n==mySectors):
		matrix=self.pl.makeX36()
            else:
                matrix=self.pl.makeX()
            for j in range(mySectors):
                matrix[i][j]=1
            self.pl.addConstraint(matrix,"=",1,self.n,mySectors)
#the total population of sector's cities mustn't exceed gamma
    def const2(self,mySectors):
        for j in range(mySectors):
            if (self.n==mySectors):
		matrix=self.pl.makeX36()
            else:
                matrix=self.pl.makeX()
            for i in range(self.n):
                matrix[i][j]=self.dep.getPopulation(i)
            self.pl.addConstraint(matrix,"<=",self.cst,self.n,mySectors)

#Solution with all combinations (NxN) minmax
    def const3(self,fx):
        n=len(fx)
        k=len(fx[0])
        for i in range(len(fx)): 
            if (self.n==len(fx)):
                matrix=self.pl.makeX36()
            else:
                matrix=self.pl.makeX()         
                matrix=self.pl.makeX()
            for j in range(len(fx[i])):
                matrix[i][j]=fx[i][j]
            self.pl.add36Constraint(matrix,"<=",self.pl.z[0],fx,n,k)


    #the sum of the cities containing resources must be equal to the parameter k  
    def const4(self):
        vector=[]
        for i in range(self.n):
            vector.append(1)
        self.pl.addConstraint1D(vector,"=",self.k)

#define the constant gamma
    def constante(self,k):
	cst=(1+self.alpha)/k
        return cst*sum(self.dep.getPopulations())
#Convert Gurobi's solution to Real solution 
    def solution(self):
        f=self.pl.getX()
        result=[]
        for i in range(len(f)):
            result.append([])
            for j in range(len(f[i])):
                result[i].append(f[i][j].x)
        return result
#Print the solution on terminal
    def solutionConcrete(self,q,secteurs):
        x=self.pl.getX()
        print"L'ensemble des villes par secteur" 
        cp=0
        for j in secteurs:
            print 'Secteur:'+str(cp)+' '+str(self.dep.getVille(j))
            print 'Les villes'
            for i in range(self.n):
                if q==0:
                    if x[i][cp].x !=0:
                        print self.dep.getVille(i)
                else:
                    if x[i][j].x !=0:
                        print self.dep.getVille(i)           
            print'\n'
            cp+=1
#The mean satisfaction is defined by the sum of distances from a city to its sector divided by the number of cities belonging to that sector
    def meanSatisfactionPerSector(self,q,secteurs):
       matrix=[]
       x=self.pl.x
       k=len(secteurs)
       #If the sector contains the citie we affect the distance instead of 1
       print 'La satisfaction moyenne de chaque secteur est '
       k=0

       for j in secteurs:
           v=0
	   cp=0
           for i in range(len(x)):
               if q==1:
                   if( x[i][j].x !=0):
                       v+=self.dep.getDistV()[i][j]
                       cp+=1
               else:
                   if( x[i][k].x !=0):
                       v+=self.dep.getDistV()[i][j]
		       cp+=1
           matrix.append(v/i)
	   print "Secteur "+self.dep.getVille(j)+": "+str(v/cp)
	   k+=1
       return matrix

    #return a couple of (Setor,Citie) of the less mayor satisfied
    def getMayorLessSat(self,matrix,secteurs):
        lowSet=[]
        #create a set of cities with the lowest dist to its sectors
        for i in range(len(matrix)): #citie
            lowSet.append(np.nanmin(matrix[i]))
        #get the highest distance
        highDist=np.nanmax(lowSet)

        result=[]
        #check if there are multiple cities with the same max distance
        print "Les maires les moins satisfaits sont des villes:"
	
        for i in range(len(matrix)):#cities
	    k=0
            for j in secteurs:#city's sectors
               if matrix[i][k]==highDist:
                   print self.dep.getVilles()[i]+" (Secteur "+str(k)+": "+self.dep.getVilles()[j]+"): "+str(highDist) 
        	   result.append([i,k])
	       k+=1
        return [result,highDist]
        	
#Plot the solution
    def getSolution(self,secteurs,q):
        #instantiate PlotMap with the departement's image, solution and coord
        p=PlotMap(self.fImage,self.pl.getX(),self.dep.getCoordV(),secteurs,q)
        #plotting
        p.getMap()



#Collect solution expressed by distances
    def distanceS_V(self,q,secteurs):
        matrix=self.pl.makeX()
        x=self.pl.getX()
        #If the sector contains the citie we affect the distance instead of 1
	k=0
        for j in secteurs:
            for i in range(len(x)):
		 if q==1:
                     if( x[i][j].x !=0):
                         matrix[i][k]=self.dep.getDistV()[i][j]
                     else:
                         matrix[i][k]=np.nan
	         else:
                     if( x[i][k].x !=0):
                         matrix[i][k]=self.dep.getDistV()[i][j]
                     else:
                         matrix[i][k]=np.nan	     
	    k+=1
        return matrix
#Create a matrix containing distances between all cities and all sectors
    def fx(self,sectors):
        cities=self.dep.getDistV()
        result=[]
        for i in range(self.n):
	    result.append([])
            for j in sectors:
	        result[i].append(cities[i][j])
	return result	

#Collect the resulting sectors
    def sectorsGenerated(self):
        secteurs=[]
        for i in range(len(self.pl.r)):
            if self.pl.r[i].x!=0:
                secteurs.append(i)
        return secteurs

#Generate PL's solution
    def solution(self):
        f=self.pl.getX()
        result=[]
        for i in range(len(f)):
            result.append([])
            for j in range(len(f[i])):
                result[i].append(f[i][j].x)
        return result


    


