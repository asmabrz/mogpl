#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 14:06:15 2018

@author: asma
"""
from gurobipy import * 

class PL:
    def __init__(self,n,k,i):
      self.m = Model("Projet")
      self.nbVars=n
      self.nbSecteurs=k
      if (i==0):
          self.x=self.makeX()
      else:
          self.x=self.makeX36()
      self.obj=LinExpr(); 
      self.obj=0
      self.nbCst=0
      self.z=[]
      self.r=[]
      
#For the third question we have to create a vector of all cities that may contain resources if they are equal to 1, 0 instead
    def addVarR(self):
        for i in range(self.nbVars):
            self.r.append(self.m.addVar(0.0, 1.0, vtype=GRB.BINARY))

#Add a variable Z for minmax problem
    def addVarZ(self):
        self.z.append(self.m.addVar(vtype=GRB.CONTINUOUS,lb=0))
# Add NxK decision variable to indicate if the city belongs to sector
    def addVarX(self,n,k):
        for i in range(n):
            for j in range(k):
                self.x[i][j]=self.m.addVar(0.0, 1.0, vtype=GRB.BINARY)

#Definition of objective for Q1                
    def defineObjective1(self,m,coef):
        n=len(coef)
        if n!=self.nbVars:
            print ("Error: Number of coefficients is not enough !")
            return 0
        else:   
            for i in range(self.nbVars):
                for j in range(self.nbSecteurs):
                    self.obj+=coef[i][j]*self.x[i][j]
            self.m.setObjective(self.obj,m) 
            return 1
#Definition of objective for Q2 and Q3
    def defineObjective2(self,o,d,n,k):  
            epsilon=0.000001
	    obj2=0
	    for i in range(n):
                for j in range(k):
                    obj2+=d[i][j]*self.x[i][j]
            obj2=epsilon*obj2
	    self.m.setObjective(self.z[0]+obj2,o) 

#For the min-max problem we had to add 36x constraints
    def add36Constraint(self,a,opr,b,fx,n,k):
        sense=self.getOperator(opr)
        if(sense!=0):
            lhs=0
            for i in range(n):
                for j in range(k):
                    lhs+=a[i][j]*self.x[i][j]

            self.m.addConstr(lhs,sense,b,"c"+str(self.nbCst))
            self.nbCst+=1
            
        else:
            print"Error: new constraint was not added"
            return 0
        
#Translate a natural written constraint to a Gurobi's one
    def addConstraint(self,a,opr,b,n,k):
        sense=self.getOperator(opr)
        if(sense!=0):
            lhs=0
            for i in range(n):
                for j in range(k):
                	lhs+=a[i][j]*self.x[i][j]
            self.m.addConstr(lhs,sense,b,"c"+str(self.nbCst))
            self.nbCst+=1
            
        else:
            print"Error: new constraint was not added"
            return 0

#This constraint ensure the fact that only k betweens n decision variables will be chosen
    def addConstraint1D(self,a,opr,b):
	sense=self.getOperator(opr)
        if(sense!=0):
            lhs=0
            for i in range(self.nbVars):
                lhs+=a[i]*self.r[i]
            self.m.addConstr(lhs,sense,b,"c"+str(self.nbCst))
            self.nbCst+=1

#translate the operator into a Gurobi operator
    def getOperator(self,o):
        if o==">=": 
            return GRB.GREATER_EQUAL
        elif o=="<=":
            return GRB.LESS_EQUAL
        elif o=="=":
            return GRB.EQUAL
        print"Error: Operator is not correct"
        return 0

#return the solution of the PL
    def getX(self):
        return self.x
#make a 36xk matrix
    def makeX(self):
        matrix=[]
        for row in range(self.nbVars):
                matrix.append([])
                for column in range(self.nbSecteurs):
                    matrix[row].append(0)
        return matrix
#make a 36x36 matrix                   
    def makeX36(self):
        matrix=[]
        for row in range(self.nbVars):
                matrix.append([])
                for column in range(self.nbVars):
                    matrix[row].append(0)
        return matrix
#generate a solution related attributes of the model
    def optimize(self):
        self.m.optimize()


    def const5(self):
	sense=self.getOperator('<=')
	for i in range(self.nbVars):
		for j in range(self.nbVars):
			lhs=self.x[i][j]
			b=self.r[j]
			self.m.addConstr(lhs,sense,b,"c"+str(self.nbCst))
            		self.nbCst+=1
    #This constraint is never used, we wrote it to confirm that it doenst depends on a constraint to express the fact that resources shoud contain themselves
    def const6(self,secteurs):
	sense=self.getOperator('=')
	for j in range(len(secteurs)):
		lhs=self.x[secteurs[j]][j]
		b=1
		self.m.addConstr(lhs,sense,b,"c"+str(self.nbCst))
            	self.nbCst+=1




