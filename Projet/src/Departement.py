#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 21:10:00 2018

@author: asma
"""

import re


class Departement:
    #Attributes are private
    def __init__(self,fVilles,fCoordVilles,fPopulations,fDistances):
        self.__villes=self.setVilles(fVilles)
        self.__coordV=self.setCoord(fCoordVilles)
        self.__populations=self.setPopulations(fPopulations)
        self.__distV=self.setDistance(fDistances)
        self.__nbVilles=self.setNbVilles()
    #getters & setters
    def getVille(self,i):
        return self.__villes [i]
    def getVilles(self):
        return self.__villes
    def getPopulations(self):
        return self.__populations
    def getCoordV(self):
        return self.__coordV
    def getDistV(self):
        return self.__distV
    def getNbVilles(self):
        return self.__nbVilles
            

    def setVilles(self,fichier):
        try:
            villes=[]
            with open(fichier, 'r') as f:
                for l in f:
                    l = l.rstrip()
                    if re.match("^[a-zA-Z_\-\s']+$",l): 
                        villes.append(l)
            return villes
        except IOError:
            print ("Error: File does not appear to exist.")
            return []
        
    def setPopulations(self,fichier):
        try:
            d=[]
            with open(fichier, 'r') as f:
                for l in f:
                    l = l.rstrip()
                    if re.match("^[a-zA-Z0-9_\-\s',]+$",l): 
                       ville,population=l.split(",")
                       d.append(float(population))
            return d
        except IOError:
          print ("Error: File does not appear to exist.")
          return []
    
    def setCoord(self,fichier):
        try:
            d=[]
            with open(fichier, 'r') as f:
                for l in f:
                    l = l.rstrip()
                    if re.match("^[a-zA-Z0-9_\-\s',]+$",l): 
                       ville,c1,c2=l.split(",")
                       d.append([float(c1),float(c2)])
            return d
        except IOError:
          print ("Error: File does not appear to exist.")
          return []
    
    def setDistance(self,fichier):
        try:
            d=[]
            with open(fichier, 'r') as f:
                for l in f:
                    l = l.rstrip()
                    if re.match("^[a-zA-Z_\-\s']+$",l): #match ville
                        a=[]
                        d.append(a)
                    if re.match("^[(\d+\.\d+)|(\d)]+$",l): #match float or int
                        a.append(float(l) )
            return d
        except IOError:
          print ("Error: File does not appear to exist.")
          return []
    
    
    def setNbVilles(self):
        return len(self.__villes)  

    def getPopulation(self,i):
        return self.getPopulations()[i]
