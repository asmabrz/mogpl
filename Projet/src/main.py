#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 22:10:02 2018

@author: asma
"""
#import matplotlib.pyplot as plt
from Builder import *
from Departement import *
from PL import *
from gurobipy import * 
#import matplotlib.image as mpimg

#Specify data's location
fImage="../data/92.png"
fVilles="../data/villes92.txt"
fCoordVilles="../data/coordvilles92.txt"
fPopulations="../data/populations92.txt"
fDistances="../data/distances92.txt"

#Definition of sectors
secteurs3=[2,22,26]
secteurs4=[2,22,26,5]
secteurs5=[2,22,26,5,10]

#Creation of the departement
dep=Departement(fVilles,fCoordVilles,fPopulations,fDistances)


#Build Model
b1=Builder(fImage,dep) 
b1.question2(secteurs4)

#PE 
'''
fx=b1.valuateSolution(b1.solution())

b2=Builder(fImage,dep) 
b2.question2(secteurs5)
gx=b2.valuateSolution(b2.solution())


def prixEquite(f,g):
        return 1-(f/g)

print prixEquite(fx,gx)'''
