import sys
import matplotlib.pyplot as plt
palette=['b','g','r','c','m','y','k']
xy= sys.argv[1].split(',')

result=[]
for i in range(len(xy)):
    result.append(xy[i].split('+'))
    result[i].pop()
print (len(result))
secteurs=sys.argv[2].split(',')
ima=sys.argv[3]
img=plt.imread(ima)
implot=plt.imshow(img)

s=[]
for i in range(len(secteurs)):
    x1,y1=secteurs[i].split('-')
    s.append([float(x1),float(y1)])
    

for j in range(len(result)):
    x1=s[j][0]
    y1=s[j][1]

    for i in range(len(result[j])):

        c1,c2=result[j][i].split('-')
        x2=float(c1)
        y2=float(c2)
        plt.plot(x2,y2,'ro')
        plt.plot([x1,x2],[y1,y2],'k-')
for i in range(len(s)):
    plt.plot(s[i][0],s[i][1],'bX')
plt.show()
